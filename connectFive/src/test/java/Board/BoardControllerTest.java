/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Board;

import Observer.Observer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ja
 */
public class BoardControllerTest {
    
    public BoardControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addButtonClickedObserverToBoard method, of class BoardController.
     */
/*   @Test
    public void testAddButtonClickedObserverToBoard() {
        System.out.println("addButtonClickedObserverToBoard");
        Observer ob = null;
        BoardController instance = null;
        instance.addButtonClickedObserverToBoard(ob);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }


    @Test
    public void testCheckWin() {
        System.out.println("checkWin");
        BoardController instance = null;
        instance.checkWin();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

*/
    @Test
    public void testMoveCamera() {
        System.out.println("moveCamera");
        int xChunkUpperLeftVisibleCorner = 0;
        int yChunkUpperLeftVisibleCorner = 0;
        int xStartView=-3;
        int yStartView=-3;
        BoardModel BM=new BoardModel(0,0);
        BoardView BV=new BoardView(BM,xStartView,yStartView,4,4);
        BoardController instance = new BoardController(BM,BV);
        assertEquals(xStartView, BV.VievX);
        assertEquals(yStartView, BV.VievY);
        instance.moveCamera(xChunkUpperLeftVisibleCorner, yChunkUpperLeftVisibleCorner);
        int modifierToCenterCamera=2;
        assertEquals(xChunkUpperLeftVisibleCorner-modifierToCenterCamera, BV.VievX);
        assertEquals(yChunkUpperLeftVisibleCorner-modifierToCenterCamera, BV.VievY);
    }

    @Test
    public void testCheckWinInLineHorizontaly() {
        System.out.println("checkWinInLine");
        int x = 0;
        int y = 1;
        int xmod = 0;
        int ymod = 1;
        BoardModel BM=new BoardModel(0,0);
        ChunkModel c1 = BM.getChunk(0, 0);
        ChunkModel c2 = BM.getChunk(0, 1);
        for(int i=0;i<c1.getChunkSize();i++){
            c1.setState(0,i,'O');
        }
        
        c1.saveChunk();
        BoardView BV=new BoardView(BM,0,0,4,4);
        ChunkModel passedChunk=BM.getChunk(0, 0);
        BoardController instance = new BoardController(BM,BV);
        boolean expResult1 = false;
        boolean result1 = instance.checkWinInLine(x, y, passedChunk, xmod, ymod);
        assertEquals(expResult1, result1); //4 w linii
        c2.setState(0,0,'O');
        c2.saveChunk();
        boolean expResult2 = true;
        boolean result2 = instance.checkWinInLine(x, y, passedChunk, xmod, ymod);
        assertEquals(expResult2, result2); //5 w linii
    }
    @Test
    public void testCheckWinInLineVerticaly() {
        System.out.println("checkWinInLine");
        int x = 1;
        int y = 0;
        int xmod = 1;
        int ymod = 0;
        BoardModel BM=new BoardModel(0,0);
        ChunkModel c1 = BM.getChunk(0, 0);
        ChunkModel c2 = BM.getChunk(1, 0);
        for(int i=0;i<c1.getChunkSize();i++){
            c1.setState(i,0,'O');
        }
        
        c1.saveChunk();
        BoardView BV=new BoardView(BM,0,0,4,4);
        ChunkModel passedChunk=BM.getChunk(0, 0);
        BoardController instance = new BoardController(BM,BV);
        boolean expResult1 = false;
        boolean result1 = instance.checkWinInLine(x, y, passedChunk, xmod, ymod);
        assertEquals(expResult1, result1); //4 w linii
        c2.setState(0,0,'O');
        c2.saveChunk();
        boolean expResult2 = true;
        boolean result2 = instance.checkWinInLine(x, y, passedChunk, xmod, ymod);
        assertEquals(expResult2, result2); //5 w linii
    }
    @Test
    public void testCheckWinInLine_Left_UP_to_Right_DOWN() {
        System.out.println("checkWinInLine");
        int x = 2;
        int y = 1;
        int xmod = -1;
        int ymod = 1;
        BoardModel BM=new BoardModel(0,0);
        ChunkModel c1 = BM.getChunk(1, 0);
        ChunkModel c2 = BM.getChunk(0,1 );
        for(int i=0;i<c1.getChunkSize();i++){
            c1.setState(3-i,i,'O');
        }
        c1.saveChunk();
        BoardView BV=new BoardView(BM,0,0,4,4);
        ChunkModel passedChunk=BM.getChunk(1, 0);
        /*for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                System.out.print(passedChunk.getState(i, j)+"\t");
            }
            System.out.println("");
        }*/
        BoardController instance = new BoardController(BM,BV);
        boolean expResult1 = false;
        boolean result1 = instance.checkWinInLine(x, y, passedChunk, xmod, ymod);
        assertEquals(expResult1, result1); //4 w linii
        c2.setState(3,0,'O');
        c2.saveChunk();
        boolean expResult2 = true;
        boolean result2 = instance.checkWinInLine(x, y, passedChunk, xmod, ymod);
        assertEquals(expResult2, result2); //5 w linii
    }
    @Test
    public void testCheckWinInLine_Left_Down_to_Right_Up() {
        System.out.println("checkWinInLine");
        int x = 1;
        int y = 1;
        int xmod = 1;
        int ymod = 1;
        BoardModel BM=new BoardModel(0,0);
        ChunkModel c1 = BM.getChunk(0, 0);
        ChunkModel c2 = BM.getChunk(1, 1);
        for(int i=0;i<c1.getChunkSize();i++){
            c1.setState(i,i,'O');
        }
        
        c1.saveChunk();
        BoardView BV=new BoardView(BM,0,0,4,4);
        ChunkModel passedChunk=BM.getChunk(0, 0);
        BoardController instance = new BoardController(BM,BV);
        boolean expResult1 = false;
        boolean result1 = instance.checkWinInLine(x, y, passedChunk, xmod, ymod);
        assertEquals(expResult1, result1); //4 w linii
        c2.setState(0,0,'O');
        c2.saveChunk();
        boolean expResult2 = true;
        boolean result2 = instance.checkWinInLine(x, y, passedChunk, xmod, ymod);
        assertEquals(expResult2, result2); //5 w linii
    }
}
