/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyWindowFacade;

import MyWindow.MyWindow;

/**
 *
 * @author Takedown
 */
public class MyWindowFacade {
    MyWindow mywindow;
     public MyWindowFacade(MyWindow mywindow) {
         this.mywindow = mywindow;
     }
     
     public void startGame() {
        this.mywindow.remove(mywindow.getMenuView());
        this.mywindow.add(mywindow.getBoardPanel());
        this.mywindow.invalidate();
        this.mywindow.validate();
        this.mywindow.repaint();
    }
    
    public void openMenu() {
        this.mywindow.remove(mywindow.getBoardPanel());
        this.mywindow.add(mywindow.getMenuView());
        this.mywindow.invalidate();
        this.mywindow.validate();
        this.mywindow.repaint();
    }  
}