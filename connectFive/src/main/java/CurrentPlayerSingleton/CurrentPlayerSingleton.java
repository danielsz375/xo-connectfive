/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CurrentPlayerSingleton;
import MyOverlayLayout.MyOverlayLayout;
import java.awt.Color;

/**
 *
 * @author Takedown
 */
public class CurrentPlayerSingleton {
    private Color colorX = new Color(51, 204, 255);
    private Color colorO = new Color(255, 102, 102);
    private static CurrentPlayerSingleton currentPlayerInstance = null;
    private char playerSymbol;
    private boolean gameFinishedState=false;
    private int[] lastMove;
    private int tourNumber=0;
    private MyOverlayLayout mol;
    private CurrentPlayerSingleton() {
        restartCurrentPlayerSingleton();
    }
    public int getTourNumber(){
        return tourNumber;
    }
    public void setMol(MyOverlayLayout mol) {
        this.mol = mol;
    }
    public int[] getLastMove(){
        return lastMove;
    }
    public void setLastMove(int BoardX,int BoardY,int ChunkX,int ChunkY){
       tourNumber++;
       lastMove=new int[]{BoardX,BoardY,ChunkX,ChunkY};
    }
    public boolean getGameFinishedState(){
        return gameFinishedState;
    }
    public void setGameFinishedState(boolean nextState){
        gameFinishedState=nextState;
    }
    public void changePlayer() {
        if(playerSymbol == 'O') {
            playerSymbol = 'X';
        } else {
            playerSymbol = 'O';
        }
        mol.updateWinPanel();
    }
    
    public Color getPlayerColor() {
        if(this.playerSymbol == 'O') 
            return colorO;
        else
            return colorX;
    }
    
    public char getPlayerSymbol() {
        return playerSymbol;
    }
    public char getTheOtherPlayerSymbol() {
        if(this.playerSymbol == 'X')
            return 'O';
        else 
            return 'X';
    }

    
    public static CurrentPlayerSingleton getInstance() {
        if (currentPlayerInstance == null) {
            currentPlayerInstance = new CurrentPlayerSingleton();
        }
        
        return currentPlayerInstance;
    }
    
    public Color getColorX() {
        return colorX;
    }
    
    public Color getColorO() {
        return colorO;
    }
    
    public void restartCurrentPlayerSingleton() {
        setGameFinishedState(false);
        playerSymbol = 'O';
    }
}
