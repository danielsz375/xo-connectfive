/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Board;
import CurrentPlayerSingleton.CurrentPlayerSingleton;
import Observer.Observer;
import java.awt.Color;
/**
 *
 * @author Takedown
 */
public class BoardController {
    BoardModel model;
    BoardView view;
    Observer myButtonClickObserver;
    public BoardController(BoardModel addModel,BoardView addView) {
        model=addModel;
        view=addView;
        myButtonClickObserver= new Observer(){
                @Override 
                public void update(){
                    checkWin();
                }
        };
        view.addButtonClickedObserver(myButtonClickObserver);
    }
    public void addButtonClickedObserverToBoard(Observer ob){
        view.addButtonClickedObserver(ob);
    }
    public void checkWin() {
        CurrentPlayerSingleton temp = CurrentPlayerSingleton.getInstance();
        int[] globalPos=temp.getLastMove();
        if(globalPos==null)
            return;
        ChunkModel currentChunk=model.getChunk(globalPos[0], globalPos[1]);
        int x=globalPos[2],y=globalPos[3];
        
        temp.setGameFinishedState
                (
                        checkWinInLine(x, y, currentChunk,0,1) ||   /*Horizontaly*/
                        checkWinInLine(x, y, currentChunk,1,0) ||   /*Verticaly*/
                        checkWinInLine(x, y, currentChunk,1,1) ||   /*Left-Up to Right-Down*/
                        checkWinInLine(x, y, currentChunk,-1,1)     /*Left-Down to Right-Up*/
                );
    }
    public void moveCamera(int xChunkUpperLeftVisibleCorner,int yChunkUpperLeftVisibleCorner){
        view.moveCamera(xChunkUpperLeftVisibleCorner, yChunkUpperLeftVisibleCorner);
    }
    public void DistinctButton(int xboard,int yboard,int xchunk,int ychunk){
        view.changeButtonColor(xboard,yboard,xchunk,ychunk,Color.yellow);
    }
    boolean checkWinInLine(int x, int y, ChunkModel passedChunk,int xmod,int ymod) {
            int currentPlayerSymbol =passedChunk.getState(x,y);
            int numberToWin=5;      
            int sum = 1;
            
            for(int i = 1; i < numberToWin; i++) 
                if(currentPlayerSymbol ==passedChunk.getWart(x+xmod*i, y+ymod*i))
                   sum++; 
                else 
                    break;      
            for(int i = 1; i < numberToWin; i++) 
                if(currentPlayerSymbol == passedChunk.getWart(x-xmod*i, y-ymod*i)) 
                    sum++;
                else 
                    break;  

            return sum>=numberToWin;  
    }
}
