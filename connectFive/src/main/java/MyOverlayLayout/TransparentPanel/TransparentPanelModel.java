/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyOverlayLayout.TransparentPanel;

import CurrentPlayerSingleton.CurrentPlayerSingleton;

/**
 *
 * @author Ja
 */
public class TransparentPanelModel {
    private int[][] Moves;
    int storedMoves=0;
    CurrentPlayerSingleton gameData= CurrentPlayerSingleton.getInstance();
    public char getPlayerSymbol(){
        return gameData.getPlayerSymbol();
    }
    public char getTheOtherPlayerSymbol(){
        return gameData.getTheOtherPlayerSymbol();
    }
    public int getStoredMoves(){
        return storedMoves;
    }
    public void addMove(){
       if(storedMoves<10)
       storedMoves++;
       
       Moves[gameData.getTourNumber()%10]=gameData.getLastMove();
       /*for(int i=0;i<storedMoves;i++){
            for(int j=0;j<4;j++){
                System.out.print(getStoredLastMoves(i)[j]+" ");
            }
            System.out.print("\n");
       }*/
    }
    public int[] getStoredLastMoves(int index){
       if(!(index<=storedMoves))
           System.out.println("Something went wrong!");
       return Moves[((gameData.getTourNumber()-index)%10+10)%10];
    }
    public TransparentPanelModel(){
        Moves=new int[10][4];
    }
}
