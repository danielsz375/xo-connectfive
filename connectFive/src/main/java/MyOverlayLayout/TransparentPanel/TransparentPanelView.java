/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyOverlayLayout.TransparentPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

/**
 *
 * @author Ja
 */


public class TransparentPanelView extends JPanel{
    ActionListener getActionListenerForButtons(int i){
        return new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        
                    }
                };
    }
    JLabel label;
    JButton lastMoves[];
    TransparentPanelModel myData;
    int lastMOvesData=0;
    public void update(){
        if(lastMOvesData!=myData.getStoredMoves()){
            for(int i=0;i<myData.getStoredMoves();i++){
                lastMoves[i].setVisible(true);
            }
        }
        lastMOvesData=myData.getStoredMoves();
        for(int i=0;i<lastMOvesData;i++){
            int[] coord=myData.getStoredLastMoves(i);
            char playerSymbol=(i%2==0)?myData.getPlayerSymbol():myData.getTheOtherPlayerSymbol();
            String text="("+playerSymbol+")\t\t " +String.valueOf(coord[0])+" "+String.valueOf(coord[1])+" "+String.valueOf(coord[2])+" "+String.valueOf(coord[3]);
                lastMoves[i].setText(text);
            }
        label.setText("Lista "+lastMOvesData+" ostatnich ruchów");
    }
    TransparentPanelControler myControler;
    void addControler(TransparentPanelControler TPcontroler){
        myControler=TPcontroler;
    }
    public TransparentPanelView(JButton menuButton,TransparentPanelModel data){
        super(new BorderLayout());
        myData=data;
        setOpaque(false);
        setBorder(new LineBorder(Color.BLACK));
        setVisible(true);
        JPanel tmpPanel = new JPanel();
        tmpPanel.setBackground(new Color(115, 215, 135, 200));
        menuButton.setPreferredSize(new Dimension(300, 100));
        menuButton.setFont(new Font("Arial", Font.PLAIN, 32));
        tmpPanel.add(menuButton);
        
        label = new JLabel("Lista ostatnich ruchów"); //troche logiki
        label.setFont(new Font("Arial", Font.PLAIN, 20));
        tmpPanel.add(label);
        lastMoves = new JButton[10];
        for(int i = 0; i < 10; i++) {
            
            lastMoves[i] = new JButton("btn");
            lastMoves[i].setPreferredSize(new Dimension(300, 50));
            lastMoves[i].setVisible(false);
            lastMoves[i].addActionListener(getActionListener(i));
            tmpPanel.add(lastMoves[i]);
        }
        
        tmpPanel.setPreferredSize(new Dimension(300, 0));
        
        add(tmpPanel, BorderLayout.EAST);
    }
    ActionListener getActionListener(int i){
        return new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                       int[] coord=myData.getStoredLastMoves(i);
                       myControler.askBoardToMoveCamera(coord[0],coord[1],coord[2],coord[3]); 
                            
                        
                    }
                };
    }
    
}

        
