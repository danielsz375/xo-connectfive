/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyGame;
import javax.swing.SwingUtilities;
import Board.BoardView;
import Board.BoardModel;
import Menu.Menu;
import MyOverlayLayout.MyOverlayLayout;
import Board.BoardController;
import CurrentPlayerSingleton.CurrentPlayerSingleton;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JFrame;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.border.EmptyBorder;
import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;


public class MyGame {
    private String title;
    private Menu menu;
    private MyOverlayLayout mol;
    private CurrentPlayerSingleton cps;
    public static void main(String[] args) {
        MyGame game = new MyGame
                .Builder()
                .title("ConnectFive")
                .withMyOverlayLayout(new MyOverlayLayout())
                .withMenu(new Menu())
                .withGameState()
                .build();
    }
    

    public static final class Builder {
        private String title;
        private Menu menu;
        private MyOverlayLayout mol;
        private CurrentPlayerSingleton cps;
        
        public Builder title(String title) {
            this.title = title;  
            return this;
        }
        
        public Builder withMyOverlayLayout(MyOverlayLayout mol) {
            this.mol = mol;
            return this;
        }
        
        public Builder withMenu(Menu menu) {
            this.menu = menu;
            return this;
        }
        
        public Builder withGameState() {
            this.cps = CurrentPlayerSingleton.getInstance();
            cps.setMol(this.mol);
            return this;
        }
        
        public MyGame build() {
            MyGame mygame = new MyGame();
            mygame.title = this.title;
            mygame.menu = this.menu;
            mygame.mol = this.mol;
            mygame.cps = this.cps;
            return mygame;
        }
    }
   
}
