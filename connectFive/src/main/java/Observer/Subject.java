/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Observer;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Ja
 */
public class Subject {
    private List<Observer> observers;
    public void attach(Observer ob){
        observers.add(ob);
    }
    public void detach(Observer ob){
        
    }
    public Subject(){
        observers =new LinkedList<Observer>();
    }
    public void myNotify(){
        for(Observer ob:observers){
            ob.update();
        }
    }
    
}
